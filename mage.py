import os
import yaml
from pathlib import Path

# commands:
# run
# new


# mage detect undefined routers, controllers, models, repositories file
# folders : controllers, models, repositories, routers
# https://docs.python.org/2/tutorial/modules.html#importing-from-a-package __all__

# TODO! auto generate SQL

class RapidMage:

    registered_repositories = []

    def __init__(self, spells):
        self.spells = spells

        with open(self.spells, 'r') as stream:
            try:
                self.parsed_yaml = yaml.safe_load(stream)

            except yaml.YAMLError as exc:
                print(exc)

    def to_camel_case(self, snake_str):
        components = snake_str.split('_')
        # We capitalize the first letter of each component except the first one
        # with the 'title' method and join them together.
        return ''.join(x.title() for x in components[0:])


    def touch(self, file_route, content=[], write_method="w"):
        print("touch", file_route)
        f = open(file_route, write_method)
        f.writelines(content)
        f.close()


    def create_module_if_not_exist(self, path):
        is_exist = os.path.exists(path)

        if not is_exist:
            os.makedirs(path)

            init_file = "{}/__init__.py".format(path)
            self.touch(init_file)

            print("create module ", path, init_file)


    def update_repositories(self, parsed_yaml):
        configs = parsed_yaml['repositories']
        migrations = configs['migrations']

        path = './src/repositories'
        self.create_module_if_not_exist(path)

        for mig in migrations:
            repo_folder = "{}/{}_repository".format(path, mig['name'])
            self.registered_repositories.append(mig['name'])
            print(repo_folder)
            self.create_module_if_not_exist(repo_folder)
            self.touch("{}/__init__.py".format(repo_folder), [
                "from .find import find\n",
                "from .add import add\n",
                "from .save import save\n",
                "from .remove import remove\n",
            ])

            self.touch("{}/find.py".format(repo_folder), [
                "def find():\n",
                "    return {'message': 'find'}\n",
            ])
            self.touch("{}/add.py".format(repo_folder), [
                "def add():\n",
                "    return {'message': 'add'}\n",
            ])
            self.touch("{}/save.py".format(repo_folder), [
                "def save():\n",
                "    return {'message': 'save'}\n",
            ])
            self.touch("{}/remove.py".format(repo_folder), [
                "def remove():\n",
                "    return {'message': 'remove'}\n",
            ])

        print("update repositories", configs)


    def update_routes(self, parsed_yaml):
        routes = parsed_yaml['routes']
        routes_path = './src/routers'
        controllers_path = './src/controllers'
        self.create_module_if_not_exist(routes_path)
        self.create_module_if_not_exist(controllers_path)

        route_names = []

        added_controller_groups = []

        for route in routes:
            is_write_file = False

            if 'controller_group' in route:
                is_write_file = True

                http_method = None
                route_path = None

                if "get" in route:
                    http_method = "get"
                    route_path = route["get"]
                elif "post" in route:
                    http_method = "post"
                    route_path = route["post"]
                elif "put" in route:
                    http_method = "put"
                    route_path = route["put"]
                elif "delete" in route:
                    http_method = "delete"
                    route_path = route["delete"]

                if http_method is not None:

                    controller_group_name = route['controller_group']

                    controller_path = "{}/{}_controller" \
                        .format(controllers_path, controller_group_name)

                    self.create_module_if_not_exist(controllers_path)

                    route_name = "{}_route".format(controller_group_name)
                    route_names.append(route_name)

                    route_file = "{}/{}_route.py" \
                        .format(routes_path, controller_group_name)

                    if controller_group_name not in added_controller_groups:
                        added_controller_groups.append(controller_group_name)
                        self.touch(route_file, [
                            "from fastapi import Request, APIRouter, Response, status\n",
                            "\n",
                            "router = APIRouter()\n",
                            "\n",
                            "\n",
                            "class {}Route:\n"
                            .format(self.to_camel_case(controller_group_name)),
                        ])

                    self.touch(route_file, [
                        "\n",
                        "    @router.{}(\"{}\")\n".format(http_method, route_path),
                        "    async def {}():\n".format(http_method),
                        "        return {'message': 'default message'}\n",
                    ], "a")

                    print("create controller_group", controller_group_name)

            elif 'repository' in route:
                is_write_file = True
                controller_group_name = route['repository']['name']

                if controller_group_name not in added_controller_groups:

                    controller_path = "{}/{}_controller"\
                        .format(controllers_path, controller_group_name)
                    self.create_module_if_not_exist(controllers_path)
                    self.touch("{}/__init__.py".format(controller_path))

                    route_name = "{}_route".format(controller_group_name)
                    route_names.append(route_name)

                    route_file = "{}/{}_route.py"\
                        .format(routes_path, controller_group_name)

                    self.touch(route_file, [
                        "from fastapi import Request, APIRouter, Response, status\n",
                        "from controllers import {}_controller\n".format(controller_group_name),
                        "\n",
                        "router = APIRouter()\n",
                        "\n",
                        "\n",
                        "class {}Route:\n"
                        .format(self.to_camel_case(controller_group_name)),
                    ])

                    # GET
                    self.touch(route_file, [
                        "\n",
                        "    @router.{}(\"{}\")\n".format("get", ""),
                        "    async def {}():\n".format("get"),
                        "        return {}_controller.get()\n".format(controller_group_name),
                    ], "a")
                    self.touch("{}/get.py".format(controller_path), [
                        "from repositories import {}_repository\n".format(controller_group_name),
                        "\n",
                        "def get():\n",
                        "    return {'message': 'get'}\n",
                    ])
                    self.touch("{}/__init__.py".format(controller_path), [
                        "from .get import get\n",
                    ], "a")

                    # GET BY UUID
                    self.touch(route_file, [
                        "\n",
                        "    @router.{}(\"{}\")\n".format("get", "/{uuid}"),
                        "    async def {}():\n".format("get_by_uuid"),
                        "        return {'message': 'default message get_by_uuid'}\n",
                    ], "a")
                    self.touch("{}/get_by_uuid.py".format(controller_path), [
                        "def get_by_uuid():\n",
                        "    return {'message': 'get'}\n",
                    ])
                    self.touch("{}/__init__.py".format(controller_path), [
                        "from .get_by_uuid import get_by_uuid\n",
                    ], "a")

                    # POST
                    self.touch(route_file, [
                        "\n",
                        "    @router.{}(\"{}\")\n".format("post", ""),
                        "    async def {}():\n".format("post"),
                        "        return {}_controller.post()\n".format(controller_group_name),
                    ], "a")
                    self.touch("{}/post.py".format(controller_path), [
                        "def post():\n",
                        "    return {'message': 'get'}\n",
                    ])
                    self.touch("{}/__init__.py".format(controller_path), [
                        "from .post import post\n",
                    ], "a")

                    self.touch(route_file, [
                        "\n",
                        "    @router.{}(\"{}\")\n".format("put", "/{uuid}"),
                        "    async def {}():\n".format("put"),
                        "        return {'message': 'default message put'}\n",
                    ], "a")

                    self.touch("{}/put.py".format(controller_path))

                    self.touch(route_file, [
                        "\n",
                        "    @router.{}(\"{}\")\n".format("delete", "/{uuid}"),
                        "    async def {}():\n".format("delete"),
                        "        return {'message': 'default message delete'}\n",
                    ], "a")

                    self.touch("{}/delete.py".format(controller_path))

                    print("create controller_group", controller_group_name)
            else:
                pass

            if is_write_file:
                pass


        fn = lambda x: "app.include_router({}.router, prefix=\"/{}\", tags=[\"{}\"])\n".format(x, x.rstrip("_route"), x.rstrip("_route"))

        self.touch("./src/main.py", [
            "from fastapi import FastAPI\n",
            "from routers import {}\n".format(", ".join(route_names)),
            "\n",
            "app = FastAPI(\n",
            "   title='Jabar Geotagging'\n",
            ")\n",
            "\n",
        ] + list(map(fn, route_names)))

        print("update routes")


    def update_app(self, parsed_yaml):
        pass

    # CAST : Generate all files
    def cast(self):
        self.update_app(self.parsed_yaml)
        self.update_repositories(self.parsed_yaml)
        self.update_routes(self.parsed_yaml)

    # MIGRATE : DB migrations
    def migrate(self):
        repositories = self.parsed_yaml['repositories']
        migrations = repositories['migrations']
        print("migrate repositories", migrations)

        sql_statement = []

        geom_columns = []

        for table_index, table in enumerate(migrations):

            if table_index > 0:
                sql_statement.append("\n")

            sql_statement.append("CREATE TABLE IF NOT EXISTS {} (\n".format(table['name']))

            for column_index, column in enumerate(table['columns']):
                column_key = next(iter(column))

                if 'geom' in column[column_key]:
                    geom_columns.append({
                        'table_name': table['name'],
                        'column_name': column_key,
                        'srid': 4326,
                        'type': 'POINT',
                        'dimension': 2,
                    })
                else:
                    sql_statement.append("  {}{} {}\n".format("," if column_index > 0 else "", column_key, column[column_key].upper(),))

            sql_statement.append(");\n")


        print("geom columns", geom_columns)

        sql_statement.append("\n")

        for geom in geom_columns:
            sql_statement.append("SELECT AddGeometryColumn ('{}', '{}', {}, '{}', {});\n".format(
                geom['table_name'],
                geom['column_name'],
                geom['srid'],
                geom['type'],
                geom['dimension'],
            ))

        self.touch("./migrations.sql", sql_statement, "w")

mage = RapidMage('./spells.yml')
# mage.cast()
mage.migrate()
